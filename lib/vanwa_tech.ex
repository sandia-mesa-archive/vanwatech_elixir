defmodule VanwaTech do
  @moduledoc """
  Simple functions for interacting with the [VanwaTech API][1].

  [1]: https://vanwa.tech/api/
  """

  @doc """
  Check what your IP address is.
  """
  def what_is_my_ip do
    params = %{"action" => "whatismyip"}
    VanwaTech.Client.get("/", params)
  end

  @doc """
  Check if a website is online and how fast it is loading.
  """
  @spec check_website_health(domain :: String.t()) :: {:ok, any()} | {:error, any()}
  def check_website_health(domain) do
    params = %{"action" => "websitehealthcheck", "url" => domain}
    VanwaTech.Client.get("/", params)
  end

  @doc """
  Test if a port is open on either an IP address or a domain.
  """
  @spec test_open_port(ip :: String.t(), port :: integer) :: {:ok, any()} | {:error, any()}
  def test_open_port(ip, port) do
    params = %{"action" => "openporttest", "ip" => ip, "port" => port}
    VanwaTech.Client.get("/", params)
  end

  @doc """
  Get WHOIS information for a domain.
  """
  @spec get_whois_info(domain :: String.t()) :: {:ok, any()}
  def get_whois_info(domain) do
    params = %{"action" => "domainwhois", "domain" => domain}
    VanwaTech.Client.get("/", params)
  end

  @doc """
  Get the IP address of a domain.
  """
  @spec get_ip_address(domain :: String.t()) :: {:ok, any()}
  def get_ip_address(domain) do
    params = %{"action" => "domaintoip", "domain" => domain}
    VanwaTech.Client.get("/", params)
  end

  @doc """
  Do a reverse lookup of an IP address.
  """
  @spec reverse_lookup(ip :: String.t()) :: {:ok, any()}
  def reverse_lookup(ip) do
    params = %{"action" => "reverseip", "ip" => ip}
    VanwaTech.Client.get("/", params)
  end

  @doc """
  Get details about a specific IPFS hash.
  """
  @spec get_ipfs_hash(hash :: String.t()) :: {:ok, any()}
  def get_ipfs_hash(hash) do
    params = %{"action" => "ipfsbrowser", "hash" => hash}
    VanwaTech.Client.get("/", params)
  end
end
