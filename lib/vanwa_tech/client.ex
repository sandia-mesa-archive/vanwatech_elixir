defmodule VanwaTech.Client do
  @moduledoc """
  API client for the VanwaTech API.
  """
  def get(path, params \\ %{}, body \\ "", headers \\ []) do
    request(:get, path, params, body, headers)
  end

  def request(method, path, params \\ %{}, body \\ "", headers \\ []) do
    http_client = get_http_client()
    endpoint = get_endpoint()

    request = %HTTPoison.Request{
      method: method,
      url: endpoint <> path,
      body: body,
      headers: headers,
      options: [],
      params: params
    }

    with {:ok, %HTTPoison.Response{body: body}} <- http_client.request(request) do
      VanwaTech.Parser.parse(body)
    end
  end

  def get_http_client do
    Application.get_env(:vanwatech, :http_client, HTTPoison)
  end

  def get_endpoint do
    Application.get_env(:vanwatech, :endpoint)
  end
end
