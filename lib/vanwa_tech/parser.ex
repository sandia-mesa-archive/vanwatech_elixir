defmodule VanwaTech.Parser do
  @moduledoc """
  Converts VanwaTech API responses into native structs.
  """

  def parse(body) do
    with {:ok, json} <- Jason.decode(body) do
      parse_json(json)
    end
  end

  defp parse_json(%{"status" => status, "data" => data}) when status == "Failure" do
    response = %VanwaTech.Error{
      status: status,
      data: data
    }

    {:error, response}
  end

  defp parse_json(%{"status" => status, "data" => data}) do
    response = %VanwaTech.Response{
      status: status,
      data: data
    }

    {:ok, response}
  end

  defp parse_json(json) do
    {:error, {:unimplemented, json}}
  end
end
