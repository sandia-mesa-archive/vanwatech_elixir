defmodule VanwaTech.Response do
  @moduledoc """
  Represents a response returned by the API.
  """
  @type t :: %__MODULE__{status: String.t(), data: map() | String.t()}

  defstruct status: "", data: %{}
end
