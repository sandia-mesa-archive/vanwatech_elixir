defmodule VanwaTech.Error do
  @moduledoc """
  Represents an error returned by the API.
  """
  @type t :: %__MODULE__{status: String.t(), data: String.t()}

  defstruct status: "", data: ""
end
