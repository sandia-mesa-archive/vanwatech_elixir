# VanwaTech Elixir

Client library for the [VanwaTech API](https://vanwa.tech/api).

## Features

### Web Tools API

This allows for performing certain basic tasks like looking up a domain, checking the health of a website,
and lookup information on a IPFS hash.

Although we've implemented most of the Web Tools API, there's a couple of API features that haven't been implemented.

#### What's been implemented

- What is my IP?
- Website Health Check
- Open Port Test
- Domain WHOIS
- Domain to IP
- Reverse IP
- IPFS Lookup

#### What's not been implemented

- IPFS Uploader
- Website Screen Capture

## Installation

The package can be installed by adding `vanwatech` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:vanwatech, "~> 0.1.0"}
  ]
end
```

### Configuration

After installing, you will need to configure VanwaTech for your application.

```elixir
import Config

config :vanwatech,
  endpoint: "https://api.vanwa.tech"
```

#### Supported options

- `endpoint` (**required**): The endpoint to use for requests. Set this to VanwaTech's API endpoint in production. There is no default for safety reasons.

## License

VanwaTech Elixir is licensed under the MIT License.
See `LICENSE.md` for a full copy of the license.