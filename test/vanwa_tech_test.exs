defmodule VanwaTechTest do
  use ExUnit.Case
  doctest VanwaTech

  test "what_is_my_ip/0 returns an IP address" do
    expected = %VanwaTech.Response{
      status: "Success",
      data: %{
        "ip" => "1.1.1.1"
      }
    }

    assert VanwaTech.what_is_my_ip() == {:ok, expected}
  end

  test "check_website_health/1 returns the loadtime on success" do
    expected = %VanwaTech.Response{
      status: "Success",
      data: %{
        "loadtime" => 0.377421
      }
    }

    assert VanwaTech.check_website_health("https://vanwa.tech/") == {:ok, expected}
  end

  test "check_website_health/1 returns an error upon failure" do
    expected = %VanwaTech.Error{
      status: "Failure",
      data: "That website did not respond in 5 seconds."
    }

    assert VanwaTech.check_website_health("https://vanwa.topl/") == {:error, expected}
  end

  test "test_open_port/2 returns the loadtime on success" do
    expected = %VanwaTech.Response{
      status: "Success",
      data: %{
        "loadtime" => 0.036
      }
    }

    assert VanwaTech.test_open_port("vanwa.tech", 443) == {:ok, expected}
  end

  test "test_open_port/2 returns an error upon failure" do
    expected = %VanwaTech.Error{
      status: "Failure",
      data: "That IP and port did not respond in 5 seconds."
    }

    assert VanwaTech.test_open_port("vanwa.tech", 19099) == {:error, expected}
  end

  test "get_whois_info/1 returns WHOIS info" do
    expected = %VanwaTech.Response{
      status: "Success",
      data: %{
        "whois" =>
          " Domain Name: VANWATECH.COM\n Registry Domain ID: 2396981845_DOMAIN_COM-VRSN\n Registrar WHOIS Server: whois.google.com\n Registrar URL: http:\/\/domains.google.com\n Updated Date: 2020-05-31T10:36:02Z\n Creation Date: 2019-05-31T04:01:37Z\n Registry Expiry Date: 2021-05-31T04:01:37Z\n Registrar: Google LLC\n Registrar IANA ID: 895\n Registrar Abuse Contact Email: registrar-abuse@google.com\n Registrar Abuse Contact Phone: +1.8772376466\n Domain Status: clientTransferProhibited https:\/\/icann.org\/epp#clientTransferProhibited\n Name Server: NS-CLOUD-E1.GOOGLEDOMAINS.COM\n Name Server: NS-CLOUD-E2.GOOGLEDOMAINS.COM\n Name Server: NS-CLOUD-E3.GOOGLEDOMAINS.COM\n Name Server: NS-CLOUD-E4.GOOGLEDOMAINS.COM\n DNSSEC: unsigned\n URL of the ICANN Whois Inaccuracy Complaint Form: https:\/\/www.icann.org\/wicf\/\n>>> Last update of whois database: 2021-03-13T19:24:26Z <<<\n\nFor more information on Whois status codes, please visit https:\/\/icann.org\/epp\n\nNOTICE: The expiration date displayed in this record is the date the\nregistrar's sponsorship of the domain name registration in the registry is\ncurrently set to expire. This date does not necessarily reflect the expiration\ndate of the domain name registrant's agreement with the sponsoring\nregistrar. Users may consult the sponsoring registrar's Whois database to\nview the registrar's reported date of expiration for this registration.\n\nTERMS OF USE: You are not authorized to access or query our Whois\ndatabase through the use of electronic processes that are high-volume and\nautomated except as reasonably necessary to register domain names or\nmodify existing registrations; the Data in VeriSign Global Registry\nServices' (\"VeriSign\") Whois database is provided by VeriSign for\ninformation purposes only, and to assist persons in obtaining information\nabout or related to a domain name registration record. VeriSign does not\nguarantee its accuracy. By submitting a Whois query, you agree to abide\nby the following terms of use: You agree that you may use this Data only\nfor lawful purposes and that under no circumstances will you use this Data\nto: (1) allow, enable, or otherwise support the transmission of mass\nunsolicited, commercial advertising or solicitations via e-mail, telephone,\nor facsimile; or (2) enable high volume, automated, electronic processes\nthat apply to VeriSign (or its computer systems). The compilation,\nrepackaging, dissemination or other use of this Data is expressly\nprohibited without the prior written consent of VeriSign. You agree not to\nuse electronic processes that are automated and high-volume to access or\nquery the Whois database except as reasonably necessary to register\ndomain names or modify existing registrations. VeriSign reserves the right\nto restrict your access to the Whois database in its sole discretion to ensure\noperational stability. VeriSign may restrict or terminate your access to the\nWhois database for failure to abide by these terms of use. VeriSign\nreserves the right to modify these terms at any time.\n\nThe Registry database contains ONLY .COM, .NET, .EDU domains and\nRegistrars.\nDomain Name: vanwatech.com\nRegistry Domain ID: 2396981845_DOMAIN_COM-VRSN\nRegistrar WHOIS Server: whois.google.com\nRegistrar URL: https:\/\/domains.google.com\nUpdated Date: 2020-05-31T10:36:02Z\nCreation Date: 2019-05-31T04:01:37Z\nRegistrar Registration Expiration Date: 2021-05-31T04:01:37Z\nRegistrar: Google LLC\nRegistrar IANA ID: 895\nRegistrar Abuse Contact Email: registrar-abuse@google.com\nRegistrar Abuse Contact Phone: +1.8772376466\nDomain Status: clientTransferProhibited https:\/\/www.icann.org\/epp#clientTransferProhibited\nRegistry Registrant ID:\nRegistrant Name: Contact Privacy Inc. Customer 1244592005\nRegistrant Organization: Contact Privacy Inc. Customer 1244592005\nRegistrant Street: 96 Mowat Ave\nRegistrant City: Toronto\nRegistrant State\/Province: ON\nRegistrant Postal Code: M4K 3K1\nRegistrant Country: CA\nRegistrant Phone: +1.4165385487\nRegistrant Phone Ext:\nRegistrant Fax:\nRegistrant Fax Ext:\nRegistrant Email: lfeqmof64fun@contactprivacy.email\nRegistry Admin ID:\nAdmin Name: Contact Privacy Inc. Customer 1244592005\nAdmin Organization: Contact Privacy Inc. Customer 1244592005\nAdmin Street: 96 Mowat Ave\nAdmin City: Toronto\nAdmin State\/Province: ON\nAdmin Postal Code: M4K 3K1\nAdmin Country: CA\nAdmin Phone: +1.4165385487\nAdmin Phone Ext:\nAdmin Fax:\nAdmin Fax Ext:\nAdmin Email: lfeqmof64fun@contactprivacy.email\nRegistry Tech ID:\nTech Name: Contact Privacy Inc. Customer 1244592005\nTech Organization: Contact Privacy Inc. Customer 1244592005\nTech Street: 96 Mowat Ave\nTech City: Toronto\nTech State\/Province: ON\nTech Postal Code: M4K 3K1\nTech Country: CA\nTech Phone: +1.4165385487\nTech Phone Ext:\nTech Fax:\nTech Fax Ext:\nTech Email: lfeqmof64fun@contactprivacy.email\nName Server: NS-CLOUD-E1.GOOGLEDOMAINS.COM\nName Server: NS-CLOUD-E2.GOOGLEDOMAINS.COM\nName Server: NS-CLOUD-E3.GOOGLEDOMAINS.COM\nName Server: NS-CLOUD-E4.GOOGLEDOMAINS.COM\nDNSSEC: unsigned\nURL of the ICANN WHOIS Data Problem Reporting System: http:\/\/wdprs.internic.net\/\n>>> Last update of WHOIS database: 2021-03-13T19:23:36.319Z <<<\n\nFor more information on Whois status codes, please visit\nhttps:\/\/www.icann.org\/resources\/pages\/epp-status-codes-2014-06-16-en\n\nPlease register your domains at: https:\/\/domains.google.com\/\nThis data is provided by Google for information purposes, and to assist\npersons obtaining information about or related to domain name registration\nrecords. Google does not guarantee its accuracy.\nBy submitting a WHOIS query, you agree that you will use this data only for\nlawful purposes and that, under no circumstances, will you use this data to:\n1) allow, enable, or otherwise support the transmission of mass\n unsolicited, commercial advertising or solicitations via E-mail (spam); or\n2) enable high volume, automated, electronic processes that apply to this\n WHOIS server.\nThese terms may be changed without prior notice.\nBy submitting this query, you agree to abide by this policy.\n\n"
      }
    }

    assert VanwaTech.get_whois_info("vanwa.tech") == {:ok, expected}
  end

  test "get_ip_address/1 returns the IP address of the domain" do
    expected = %VanwaTech.Response{
      status: "Success",
      data: "23.163.176.201"
    }

    assert VanwaTech.get_ip_address("vanwa.tech") == {:ok, expected}
  end

  test "reverse_ip/1 returns the result from the reverse lookup" do
    expected = %VanwaTech.Response{
      status: "Success",
      data: "vanwa.tech"
    }

    assert VanwaTech.reverse_lookup("23.163.176.201") == {:ok, expected}
  end

  test "get_ipfs_hash/1 returns the result if it successfully finds the hash" do
    expected = %VanwaTech.Response{
      status: "Success",
      data: %{
        "mimeType" => "text\/plain",
        "extension" => "txt",
        "fileSize" => 20
      }
    }

    assert VanwaTech.get_ipfs_hash("QmcWjMf15eyrieUHrZbsAuVEDGQt6HaaAbUB6iThGVCNow") ==
             {:ok, expected}
  end

  test "get_ipfs_hash/1 returns an error if it doesn't find the hash" do
    expected = %VanwaTech.Error{
      status: "Failure",
      data: "Invalid Action"
    }

    assert VanwaTech.get_ipfs_hash("AAAA") == {:error, expected}
  end
end
