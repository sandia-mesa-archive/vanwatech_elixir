defmodule VanwaTech.HTTPoisonMock do
  def request(%HTTPoison.Request{
        url: "https://api.vanwa.test/",
        params: %{
          "action" => "whatismyip"
        }
      }) do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/what_is_my_ip.json"),
       status_code: 200
     }}
  end

  def request(%HTTPoison.Request{
        url: "https://api.vanwa.test/",
        params: %{
          "action" => "websitehealthcheck",
          "url" => "https://vanwa.tech/"
        }
      }) do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/website_health_check.json"),
       status_code: 200
     }}
  end

  def request(%HTTPoison.Request{
        url: "https://api.vanwa.test/",
        params: %{
          "action" => "websitehealthcheck",
          "url" => "https://vanwa.topl/"
        }
      }) do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/website_health_check_error.json"),
       status_code: 200
     }}
  end

  def request(%HTTPoison.Request{
        url: "https://api.vanwa.test/",
        params: %{
          "action" => "openporttest",
          "ip" => "vanwa.tech",
          "port" => 443
        }
      }) do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/open_port_test.json"),
       status_code: 200
     }}
  end

  def request(%HTTPoison.Request{
        url: "https://api.vanwa.test/",
        params: %{
          "action" => "openporttest",
          "ip" => "vanwa.tech",
          "port" => 19099
        }
      }) do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/open_port_test_error.json"),
       status_code: 200
     }}
  end

  def request(%HTTPoison.Request{
        url: "https://api.vanwa.test/",
        params: %{
          "action" => "domainwhois",
          "domain" => "vanwa.tech"
        }
      }) do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/domain_whois.json"),
       status_code: 200
     }}
  end

  def request(%HTTPoison.Request{
        url: "https://api.vanwa.test/",
        params: %{
          "action" => "domaintoip",
          "domain" => "vanwa.tech"
        }
      }) do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/domain_to_ip.json"),
       status_code: 200
     }}
  end

  def request(%HTTPoison.Request{
        url: "https://api.vanwa.test/",
        params: %{
          "action" => "reverseip",
          "ip" => "23.163.176.201"
        }
      }) do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/reverse_ip.json"),
       status_code: 200
     }}
  end

  def request(%HTTPoison.Request{
        url: "https://api.vanwa.test/",
        params: %{
          "action" => "ipfsbrowser",
          "hash" => "QmcWjMf15eyrieUHrZbsAuVEDGQt6HaaAbUB6iThGVCNow"
        }
      }) do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/ipfs_browser.json"),
       status_code: 200
     }}
  end

  def request(%HTTPoison.Request{
        url: "https://api.vanwa.test/",
        params: %{
          "action" => "ipfsbrowser",
          "hash" => "AAAA"
        }
      }) do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/ipfs_browser_error.json"),
       status_code: 200
     }}
  end
end
