defmodule VanwaTech.MixProject do
  use Mix.Project

  def project do
    [
      app: :vanwatech,
      version: "0.1.0",
      elixir: "~> 1.11",
      name: "VanwaTech",
      description: "VanwaTech API library",
      source_url: "https://code.sandiamesa.com/sandiamesa/vanwatech_elixir",
      package: package(),
      docs: docs(),
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def package do
    [
      licenses: ["MIT"],
      links: %{"Sandia Mesa Code" => "https://code.sandiamesa.com/sandiamesa/vanwatech_elixir"}
    ]
  end

  def docs do
    [
      main: "readme",
      extras: [
        "README.md",
        "LICENSE.md"
      ],
      groups_for_modules: groups_for_modules()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Specifies which path to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:jason, "~> 1.2"},
      {:httpoison, "~> 1.8"},
      {:ex_doc, "~> 0.23.0", only: :dev, runtime: false}
    ]
  end

  defp groups_for_modules do
    []
  end
end
